package com.cookandroid.blahblar;

import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.cookandroid.blahblar.R;
import java.util.ArrayList;
import java.util.List;

public class ReviewActivity extends AppCompatActivity {
    ImageView btn_toolbar_back;
    ListView list1_3;
    Button graph_3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        btn_toolbar_back = (ImageView) findViewById(R.id.btn_toolbar_back_r);
        btn_toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final ArrayList<String> midList1_3 = new ArrayList<String>();  //오답목록 저장
        final ArrayAdapter<String> adapter1_3 = new android.widget.ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, midList1_3);


        list1_3 = (ListView) findViewById(R.id.listView1_3);
        list1_3.setAdapter(adapter1_3);

        //예시
        midList1_3.add("example1");
        midList1_3.add("example2");
        midList1_3.add("example3");
        midList1_3.add("example4");
        midList1_3.add("example5");
        adapter1_3.notifyDataSetChanged();

        //리스트 클릭 : 문항별 보기
        list1_3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int p, long l) {
                Intent intent = new Intent(ReviewActivity.this, ReviewQuestionActivity.class);
                startActivity(intent);
            }
        });

        // 리스트 길게 클릭 : 문항 삭제
        list1_3.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
                AlertDialog.Builder dlg = new AlertDialog.Builder(ReviewActivity.this);
                dlg.setTitle("정말 삭제하시겠습니까?");
                // 확인버튼 누르면
                dlg.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(),"삭제하였습니다.", Toast.LENGTH_SHORT).show();
                        midList1_3.remove(position);
                        adapter1_3.notifyDataSetChanged();
                    }
                });
                dlg.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(),"삭제를 취소하였습니다.", Toast.LENGTH_SHORT).show();
                    }
                });
                dlg.show();
                return false;
            }
        });

        graph_3 = (Button)findViewById(R.id.graph_3);
        graph_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReviewActivity.this, ReviewGraphActivity.class);
                startActivity(intent);
            }
        });
    }
}
